const counterFactory = require('../counterFactory.js');
const result = counterFactory();
const expectedIncreement = 1;
const expectedDecreement = 0;
if (expectedIncreement === result.increment() && expectedDecreement === result.decrement()) {
    console.log("Test Passed");
} else {
    console.log("Test failed");
}
