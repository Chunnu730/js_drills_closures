const cacheFunction = require('../cacheFunction.js');
const cb = () => 'invokes';
const result = cacheFunction(cb);
const expectedInvokeresult = 'invokes';
const expectedNotInvokeresult = 'args1'
const passedArguments = [result('args1') , result('args2'),result('args1')];
for (let index = 0; index < passedArguments.length; index++) {
    if (expectedInvokeresult === passedArguments[index]) {
        console.log('Test passed : '+passedArguments[index]);
    } else if (expectedNotInvokeresult === passedArguments[index]) {
        console.log('Test passed : ', passedArguments[index] );
    }
    else {
        console.log('Test failed');
    }
}