const limitFunctionCallCount = require('../limitFunctionCallCount.js');
const cb = () => 'invokes';
const result = limitFunctionCallCount(cb, 2);
for (let callTimes = 2; callTimes >= 0; callTimes--) {
    const expectedResult = 'invokes';
    const outputResult = result();
    if (expectedResult === outputResult) {
        console.log(`Test passed : ${outputResult}`);
    } else if (!outputResult) {
        console.log(`Test passed : ${outputResult}`);
    } else {
        console.log('Test failed');
    }
}

